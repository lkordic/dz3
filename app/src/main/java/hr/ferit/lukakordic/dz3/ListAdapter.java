package hr.ferit.lukakordic.dz3;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Luka on 10.4.2017..
 */

public class ListAdapter extends BaseAdapter {

    private ArrayList<Task> tasks;

    public ListAdapter (ArrayList<Task> tasks){
        this.tasks = tasks;
    }
    @Override
    public int getCount() {return this.tasks.size(); }

    @Override
    public Object getItem(int position) {
        return this.tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;

        if(convertView == null){
            parent.setBackgroundColor(Color.argb(220, 0, 0, 0));
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            convertView = layoutInflater.inflate(R.layout.list_item,parent, false);
            listViewHolder = new ViewHolder(convertView);
            convertView.setTag(listViewHolder);
        }
        else {
            listViewHolder = (ViewHolder) convertView.getTag();
        }
        Task task = this.tasks.get(position);
        listViewHolder.tvTitle.setText(task.getTitle());
        listViewHolder.tvDescription.setText(task.getDescription());
        listViewHolder.ivColor.setBackgroundColor(task.getColor());
        return convertView;
        }

    private static class ViewHolder {
        private TextView tvTitle, tvDescription;
        private ImageView ivColor;

        private ViewHolder(View listView){
            tvTitle = (TextView) listView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) listView.findViewById(R.id.tvDescription);
            ivColor = (ImageView) listView.findViewById(R.id.ivPriority);

        }
    }
    public void add(Task task){
        this.tasks.add(task);
        this.notifyDataSetChanged();
    }

    public void deleteAt(int position){
        this.tasks.remove(position);
        this.notifyDataSetChanged();
    }
}

