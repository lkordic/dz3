package hr.ferit.lukakordic.dz3;

/**
 * Created by Luka on 10.4.2017..
 */

public class Task {
    private String title;
    private String description;
    private int color;

    public Task (String title, String description, int color){
        this.title = title;
        this.description = description;
        this.color = color;
    }
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getColor() {
        return color;
    }
}
