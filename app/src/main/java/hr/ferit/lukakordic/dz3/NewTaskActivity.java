package hr.ferit.lukakordic.dz3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class NewTaskActivity extends Activity implements View.OnClickListener {
    TextView tvNewTask;
    EditText etTitle, etDescription;
    Spinner sPriority;
    String spinnerItem, color;
    Button btnAdd;
    int priority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        this.setUpUI();
    }

    private void setUpUI() {
        this.tvNewTask = (TextView) findViewById(R.id.tvNewTask);
        this.etTitle = (EditText) findViewById(R.id.etTitle);
        this.sPriority = (Spinner) findViewById(R.id.sPriority);
        this.btnAdd = (Button) findViewById(R.id.btnAdd);
        this.etDescription = (EditText) findViewById(R.id.etDescription);

        this.btnAdd.setOnClickListener(this);
        this.sPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    @Override
    public void onClick(View v) {
        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        switch (spinnerItem){
            case "Low":
                color = "#07cc00";
                break;
            case "Normal":
                color = "#fff540";
                break;
            case "High":
                color = "#df3a37";
                break;
        }
        priority = Color.parseColor(color);
        Task task = new Task(title, description, priority);
        TaskDBHelper.getInstance(this).insertTask(task);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
