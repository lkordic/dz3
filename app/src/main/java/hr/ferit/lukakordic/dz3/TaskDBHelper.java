package hr.ferit.lukakordic.dz3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import javax.xml.validation.Schema;

/**
 * Created by Luka on 11.4.2017..
 */

public class TaskDBHelper extends SQLiteOpenHelper {

    //Singleton
    private static TaskDBHelper taskDBHelper = null;

    private TaskDBHelper(Context context){
        super(context.getApplicationContext(), Schema.DATABASE_NAME, null, Schema.SCHEMA_VERSION);
    }

    public static TaskDBHelper getInstance(Context context){
        if(taskDBHelper == null){
            taskDBHelper = new TaskDBHelper(context);
        }
        return taskDBHelper;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TASKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_TASKS);
        this.onCreate(db);
    }

    //SQL
    static final String CREATE_TABLE_TASKS = "CREATE TABLE " + Schema.TABLE_TASKS +
            " (" + Schema.TITLE + " TEXT," + Schema.DESCRIPTION + " TEXT," + Schema.COLOR + " INTEGER);";

    static final String DROP_TABLE_TASKS = "DROP TABLE IF EXISTS " + Schema.TABLE_TASKS;

    static final String SELECT_ALL_TASKS = "SELECT " + Schema.TITLE + "," + Schema.DESCRIPTION + "," + Schema.COLOR + " FROM " + Schema.TABLE_TASKS;


    public void insertTask(Task task){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.TITLE, task.getTitle());
        contentValues.put(Schema.DESCRIPTION, task.getDescription());
        contentValues.put(Schema.COLOR, task.getColor());
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        writableDatabase.insert(Schema.TABLE_TASKS, Schema.TITLE, contentValues);
        writableDatabase.close();
    }

    public void deleteTask(Task task){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Schema.TABLE_TASKS, Schema.TITLE + " = ?", new String[] {String.valueOf(task.getTitle())});
        db.close();
    }

    public ArrayList<Task> getAllTasks(){
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        Cursor taskCursor = writableDatabase.rawQuery(SELECT_ALL_TASKS, null);
        ArrayList<Task> tasks = new ArrayList<>();
        if(taskCursor.moveToFirst()){
            do{
                String title = taskCursor.getString(0);
                String description = taskCursor.getString(1);
                int color = taskCursor.getInt(2);
                tasks.add(new Task(title, description, color));
            }while (taskCursor.moveToNext());
        }
        taskCursor.close();
        writableDatabase.close();
        return tasks;
    }

    public static class Schema{
        private static final int SCHEMA_VERSION = 2;
        private static final String DATABASE_NAME = "tasks.db";
        static final String TABLE_TASKS = "tasks";
        static final String TITLE = "title";
        static final String DESCRIPTION = "description";
        static final String COLOR = "priority";
    }
}

