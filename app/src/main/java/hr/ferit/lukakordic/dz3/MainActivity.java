package hr.ferit.lukakordic.dz3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    Button btnNewTask;
    ListView lvToDoList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }

    private void setUpUI() {
        this.lvToDoList = (ListView) findViewById(R.id.lvToDoList);
        final ArrayList<Task> tasks = this.loadTasks();
        final ListAdapter listAdapter = new ListAdapter(tasks);
        this.lvToDoList.setAdapter(listAdapter);

        this.btnNewTask = (Button) findViewById(R.id.btnNewTask);
        this.btnNewTask.setOnClickListener(this);

        this.lvToDoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                TaskDBHelper.getInstance(getApplicationContext()).deleteTask((Task) listAdapter.getItem(position));
                listAdapter.deleteAt(position);
                return true;
            }
        });
    }

    private ArrayList<Task> loadTasks() {
        return TaskDBHelper.getInstance(this).getAllTasks();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, NewTaskActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        this.setUpUI();
    }
}
