#Tasky#
## Aplikacija za praćenje zadataka ##

### Opis ###
Pri pokretanju aplikacije prikazuje se MainActivity sa pripadajućim sučeljem na kojemu se nalazi Lista zadataka i gumb za dodavanje novog zadatka. Zadaci se učitavaju u listu iz baze podataka. Za rad sa listom kreirana je klasa ListAdapter koja nasljeđuje klasu BaseAdapter. Za poboljšanje performansi liste dodana je i klasa ViewHolder koja drži reference na elemente sučelja. Klikom na gumb New Task pokreće se nova aktivnost za dodavanje novog zadatka. U toj aktivnosti korisnik unosi naslov i opis zadatka te njegov prioritet. Nakon unosa podataka, klikom na gumb Add, podatak se sprema u bazu i ponovno se pokreće MainActivity. Dugim pritiskom na element u listi taj se element briše iz prikaza i iz baze podataka. 

### Pomoćni materijali ###
* [Android List View](https://developer.android.com/guide/topics/ui/layout/listview.html)
* [Deleting items from database and List View](http://stackoverflow.com/questions/9645101/how-to-delete-item-from-listview)

### Screenshots ###

![Screenshot_2017-04-13-22-55-39[2].png](https://bitbucket.org/repo/AgGknzX/images/274301257-Screenshot_2017-04-13-22-55-39%5B2%5D.png)![Screenshot_2017-04-13-22-55-53[1].png](https://bitbucket.org/repo/AgGknzX/images/3558528039-Screenshot_2017-04-13-22-55-53%5B1%5D.png)![Screenshot_2017-04-13-22-56-04[1].png](https://bitbucket.org/repo/AgGknzX/images/1251616141-Screenshot_2017-04-13-22-56-04%5B1%5D.png)![Screenshot_2017-04-13-23-01-18[1].png](https://bitbucket.org/repo/AgGknzX/images/3604438197-Screenshot_2017-04-13-23-01-18%5B1%5D.png)![Screenshot_2017-04-13-23-01-25[1].png](https://bitbucket.org/repo/AgGknzX/images/808995319-Screenshot_2017-04-13-23-01-25%5B1%5D.png)